package ru.vsarychev.tm.exception;

public final class ProjectEmptyException extends AbstractException {

    public ProjectEmptyException() {
        super("Error! Project is empty...");
    }

}
