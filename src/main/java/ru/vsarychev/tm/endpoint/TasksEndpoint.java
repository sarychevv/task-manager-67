package ru.vsarychev.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.api.endpoint.ITasksEndpoint;
import ru.vsarychev.tm.api.service.ITaskService;
import ru.vsarychev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.vsarychev.tm.api.endpoint.ITasksEndpoint")
public final class TasksEndpoint implements ITasksEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @Nullable
    @GetMapping()
    @WebMethod
    public List<Task> get() {
        return taskService.findAll();
    }

    @Override
    @PostMapping
    @WebMethod
    public void post(@RequestBody @Nullable List<Task> tasks) {
        taskService.saveAll(tasks);
    }

    @Override
    @PutMapping
    @WebMethod
    public void put(@RequestBody @Nullable List<Task> tasks) {
        taskService.saveAll(tasks);
    }

    @Override
    @DeleteMapping()
    @WebMethod
    public void delete() {
        taskService.removeAll();
    }

}