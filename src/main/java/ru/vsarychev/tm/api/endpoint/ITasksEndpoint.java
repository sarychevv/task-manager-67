package ru.vsarychev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITasksEndpoint {

    @Nullable
    @GetMapping()
    @WebMethod
    List<Task> get();

    @PostMapping
    @WebMethod
    void post(@RequestBody @Nullable List<Task> tasks);

    @PutMapping
    @WebMethod
    void put(@RequestBody @Nullable List<Task> tasks);

    @DeleteMapping()
    @WebMethod
    void delete();
}
