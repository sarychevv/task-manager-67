package ru.vsarychev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vsarychev.tm.client.TaskClient;
import ru.vsarychev.tm.marker.IntegrationCategory;
import ru.vsarychev.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

@Category(IntegrationCategory.class)
public class TaskTest {

    private static final Integer SIZE = (new Random()).nextInt(10 - 1 + 1) + 1;
    @Nullable
    private static List<Task> taskList;
    @NotNull
    private final TaskClient taskClient = TaskClient.client();

    @Before
    public void before() {
        taskList = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            @NotNull Task task = new Task();
            task.setName("Task" + i);
            task.setDescription("description" + i);
            taskClient.put(task);
            taskList.add(task);
        }
    }

    @After
    public void after() {
        for (int i = 0; i < taskList.size(); i++) {
            taskClient.delete(taskList.get(i).getId());
        }
    }

    @Test
    public void get() {
        final Task task = taskClient.get(taskList.get(0).getId());
        assertNotNull(task);
    }

    @Test
    public void post() {
        @NotNull Task task = new Task();
        task.setName("TaskCreated");
        task.setDescription("DescriptionCreated");
        taskClient.post(task);
        taskList.add(task);
        assertEquals(taskClient.get(SIZE.toString()).getName(), "TaskCreated");
        assertEquals(taskClient.get(SIZE.toString()).getDescription(), "DescriptionCreated");
        after();
        before();
    }

    @Test
    public void put() {
        final Task task = taskClient.get(taskList.get(0).getId());
        task.setDescription("UpdDesc");
        task.setName("UpdName");
        taskClient.put(task);
        assertEquals(taskClient.get(taskList.get(0).getId()).getName(), "UpdName");
        assertEquals(taskClient.get(taskList.get(0).getId()).getDescription(), "UpdDesc");
    }

    @Test
    public void delete() {
        for (int i = 0; i < taskList.size(); i++) {
            taskClient.delete(taskList.get(i).getId());
        }
        assertNull(taskClient.get(taskList.get(0).getId()));
        before();
    }

}
