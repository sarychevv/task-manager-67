package ru.vsarychev.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.vsarychev.tm.api.service.IPropertyService;

@Getter
@Setter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";
    @Value("#{environment['server.port']}")
    private Integer serverPort;
    @Value("#{environment['server.host']}")
    private String serverHost;
    @Value("#{environment['session.key']}")
    private String sessionKey;
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeOut;
    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;
    @Value("#{environment['database.username']}")
    private String databaseUsername;
    @Value("#{environment['database.password']}")
    private String databasePassword;
    @Value("#{environment['database.url']}")
    private String databaseUrl;
    @Value("#{environment['password.secret']}")
    private String passwordSecret;
    @Value("#{environment['database.driver']}")
    private String dataBaseDriver;
    @Value("#{environment['database.dialect']}")
    private String dataBaseDialect;
    @Value("#{environment['database.show_sql']}")
    private String dataBaseShowSql;
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String dataBaseHbm2ddlAuto;
    @Value("#{environment['database.format_sql']}")
    private String dataBaseFormatSql;
    @Value("#{environment['database.second_lvl_cache']}")
    private String dataBaseSecondLvlCache;
    @Value("#{environment['database.factory_class']}")
    private String dataBaseFactoryClass;
    @Value("#{environment['database.use_query_cache']}")
    private String dataBaseUseQueryCache;
    @Value("#{environment['database.use_min_puts']}")
    private String dataBaseUseMinPuts;
    @Value("#{environment['database.region_prefix']}")
    private String dataBaseRegionPrefix;
    @Value("#{environment['database.config_file_path']}")
    private String dataBaseConfigFilePath;
    @Value("#{environment['jms.queue']}")
    private String jmsQueue;
    @Value("#{environment['jms.connector']}")
    private String jmsConnector;

    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

}
