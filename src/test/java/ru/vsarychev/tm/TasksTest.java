package ru.vsarychev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vsarychev.tm.client.TasksClient;
import ru.vsarychev.tm.marker.IntegrationCategory;
import ru.vsarychev.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@Category(IntegrationCategory.class)
public class TasksTest {

    private static final Integer SIZE = (new Random()).nextInt(10 - 1 + 1) + 1;
    @Nullable
    private static List<Task> taskList;
    @NotNull
    private final TasksClient tasksClient = TasksClient.client();

    @Before
    public void before() {
        taskList = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            @NotNull Task task = new Task();
            task.setName("Task" + i);
            task.setDescription("description" + i);
            taskList.add(task);
        }
        tasksClient.put(taskList);
    }

    @After
    public void after() {
        tasksClient.delete();
    }

    @Test
    public void get() {
        final List<Task> tasks = tasksClient.get();
        assertNotEquals(0, tasks.size());
    }

    @Test
    public void post() {
        final long Size = (new Random()).nextInt(10 - 1 + 1) + 1;
        final List<Task> testTasktList = new ArrayList<Task>();
        for (int i = 0; i < Size; i++) {
            @NotNull Task task = new Task();
            task.setName("Task_test" + i);
            task.setDescription("description_test" + i);
            testTasktList.add(task);
            taskList.add(task);
        }
        tasksClient.post(testTasktList);
        final long size = tasksClient.get().size();
        assertEquals(size, SIZE + Size);
        after();
        before();
    }

    @Test
    public void put() {
        final String NewName = "UpdName";
        final String NewDesc = "UpdDesc";
        for (final Task task : taskList) {
            task.setName(NewName);
            task.setDescription(NewDesc);
        }
        tasksClient.put(taskList);
        final List<Task> updTaskList = tasksClient.get();
        for (final Task task : updTaskList) {
            assertEquals(task.getName(), NewName);
            assertEquals(task.getDescription(), NewDesc);
        }
    }

    @Test
    public void delete() {
        tasksClient.delete();
        assertEquals(0, tasksClient.get().size());
        before();
    }

}
