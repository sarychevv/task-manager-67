package ru.vsarychev.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.api.endpoint.IProjectsEndpoint;
import ru.vsarychev.tm.api.service.IProjectService;
import ru.vsarychev.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.vsarychev.tm.api.endpoint.IProjectsEndpoint")
public final class ProjectsEndpoint implements IProjectsEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @Nullable
    @GetMapping
    @WebMethod
    public List<Project> get() {
        return projectService.findAll();
    }

    @Override
    @PostMapping
    @WebMethod
    public void post(@RequestBody @Nullable List<Project> projects) {
        projectService.saveAll(projects);
    }

    @Override
    @PutMapping
    @WebMethod
    public void put(@RequestBody @Nullable List<Project> projects) {
        projectService.saveAll(projects);
    }

    @Override
    @DeleteMapping
    @WebMethod
    public void delete() {
        projectService.removeAll();
    }

}