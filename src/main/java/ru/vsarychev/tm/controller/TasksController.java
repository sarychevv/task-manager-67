package ru.vsarychev.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.vsarychev.tm.api.service.IProjectService;
import ru.vsarychev.tm.api.service.ITaskService;

@Controller
public class TasksController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/tasks")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskService.findAll());
        modelAndView.addObject("projectRepository", projectService);
        return modelAndView;
    }

}
