package ru.vsarychev.tm.configuration;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.vsarychev.tm.api.endpoint.IProjectEndpoint;
import ru.vsarychev.tm.api.endpoint.IProjectsEndpoint;
import ru.vsarychev.tm.api.endpoint.ITaskEndpoint;
import ru.vsarychev.tm.api.endpoint.ITasksEndpoint;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@ComponentScan("ru.vsarychev.tm")
@EnableWebMvc
@Configuration
public class WebApplicationConfiguration implements WebMvcConfigurer, WebApplicationInitializer {

    @Bean
    public ViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass(JstlView.class);
        bean.setPrefix("/WEB-INF/views/");
        bean.setSuffix(".jsp");
        return bean;
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        final CXFServlet cxfServlet = new CXFServlet();
        final ServletRegistration.Dynamic dynamicCXF =
                servletContext.addServlet("cxfServlet", cxfServlet);
        dynamicCXF.addMapping("/ws/*");
        dynamicCXF.setLoadOnStartup(1);
    }

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    public Endpoint projectEndpointRegistry(final IProjectEndpoint projectEndpoint, SpringBus cxf) {
        final EndpointImpl endpoint = new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint projectCollectionEndpointRegistry(final IProjectsEndpoint projectsEndpoint, SpringBus cxf) {
        final EndpointImpl endpoint = new EndpointImpl(cxf, projectsEndpoint);
        endpoint.publish("/ProjectsEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(final ITaskEndpoint taskEndpoint, SpringBus cxf) {
        final EndpointImpl endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskCollectionEndpointRegistry(final ITasksEndpoint tasksEndpoint, SpringBus cxf) {
        final EndpointImpl endpoint = new EndpointImpl(cxf, tasksEndpoint);
        endpoint.publish("/TasksEndpoint");
        return endpoint;
    }

}
