package ru.vsarychev.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.model.Project;

import java.util.List;

public interface ProjectsClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/projects";

    static ProjectsClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectsClient.class, BASE_URL);
    }

    @Nullable
    @GetMapping
    List<Project> get();

    @PostMapping
    void post(@RequestBody @Nullable List<Project> projects);

    @PutMapping
    void put(@RequestBody @Nullable List<Project> projects);

    @DeleteMapping
    void delete();

}
