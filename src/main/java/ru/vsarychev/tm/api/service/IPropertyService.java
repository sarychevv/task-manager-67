package ru.vsarychev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeOut();

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseUsername();

    @NotNull
    String getDataBaseDriver();

    @NotNull
    String getDataBaseDialect();

    @NotNull
    String getDataBaseShowSql();

    @NotNull
    String getDataBaseHbm2ddlAuto();

    @NotNull
    String getDataBaseFormatSql();

    @NotNull
    String getDataBaseSecondLvlCache();

    @NotNull
    String getDataBaseFactoryClass();

    @NotNull
    String getDataBaseUseQueryCache();

    @NotNull
    String getDataBaseUseMinPuts();

    @NotNull
    String getDataBaseRegionPrefix();

    @NotNull
    String getDataBaseConfigFilePath();

    @NotNull
    String getJmsQueue();

    @NotNull
    String getJmsConnector();

}
