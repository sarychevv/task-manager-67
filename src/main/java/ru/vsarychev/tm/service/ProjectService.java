package ru.vsarychev.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vsarychev.tm.api.repository.IProjectRepository;
import ru.vsarychev.tm.api.service.IProjectService;
import ru.vsarychev.tm.exception.IdEmptyException;
import ru.vsarychev.tm.exception.ProjectEmptyException;
import ru.vsarychev.tm.exception.ProjectsEmptyException;
import ru.vsarychev.tm.model.Project;

import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @Override
    @Nullable
    public Project findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void create(@Nullable Project project) {
        if (project == null) throw new ProjectEmptyException();
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void update(@Nullable Project project) {
        if (project == null) throw new ProjectEmptyException();
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void deleteById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        projectRepository.deleteById(id);
    }

    @Override
    @Nullable
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Transactional
    public void saveAll(@Nullable List<Project> projects) {
        if (projects == null || projects.isEmpty()) throw new ProjectsEmptyException();
        projectRepository.saveAll(projects);
    }

    @Override
    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }
}
