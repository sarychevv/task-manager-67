package ru.vsarychev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vsarychev.tm.client.ProjectsClient;
import ru.vsarychev.tm.marker.IntegrationCategory;
import ru.vsarychev.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@Category(IntegrationCategory.class)
public class ProjectsTest {

    private static final Integer SIZE = (new Random()).nextInt(10 - 1 + 1) + 1;
    @Nullable
    private static List<Project> projectList;
    @NotNull
    private final ProjectsClient projectsClient = ProjectsClient.client();

    @Before
    public void before() {
        projectList = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            @NotNull Project project = new Project();
            project.setName("Project" + i);
            project.setDescription("description" + i);
            projectList.add(project);
        }
        projectsClient.put(projectList);
    }

    @After
    public void after() {
        projectsClient.delete();
    }

    @Test
    public void get() {
        final List<Project> projects = projectsClient.get();
        assertNotEquals(0, projects.size());
    }

    @Test
    public void post() {
        final long Size = (new Random()).nextInt(10 - 1 + 1) + 1;
        final List<Project> newProjectList = new ArrayList<Project>();
        for (int i = 0; i < Size; i++) {
            @NotNull Project project = new Project();
            project.setName("Project_test" + i);
            project.setDescription("description_test" + i);
            newProjectList.add(project);
            projectList.add(project);
        }
        projectsClient.post(newProjectList);
        final long size = projectsClient.get().size();
        assertEquals(size, SIZE + Size);
        after();
        before();
    }

    @Test
    public void put() {
        final String NewName = "UpdName";
        final String NewDesc = "UpdDesc";
        for (final Project project : projectList) {
            project.setName(NewName);
            project.setDescription(NewDesc);
        }
        projectsClient.put(projectList);
        final List<Project> updProjectList = projectsClient.get();
        for (final Project project : updProjectList) {
            assertEquals(project.getName(), NewName);
            assertEquals(project.getDescription(), NewDesc);
        }
    }

    @Test
    public void delete() {
        projectsClient.delete();
        assertEquals(0, projectsClient.get().size());
        before();
    }

}
