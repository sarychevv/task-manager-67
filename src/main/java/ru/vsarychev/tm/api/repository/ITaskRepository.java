package ru.vsarychev.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vsarychev.tm.model.Task;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {
}
