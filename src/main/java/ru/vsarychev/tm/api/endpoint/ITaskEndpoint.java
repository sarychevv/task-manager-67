package ru.vsarychev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ITaskEndpoint {
    @Nullable
    @GetMapping("/{id}")
    @WebMethod
    Task get(@PathVariable("id") @Nullable String id);

    @PostMapping
    @WebMethod
    void post(@RequestBody @Nullable Task task);

    @PutMapping
    @WebMethod
    void put(@RequestBody @Nullable Task task);

    @DeleteMapping("/{id}")
    @WebMethod
    void delete(@PathVariable("id") @Nullable String id);
}
