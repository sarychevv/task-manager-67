package ru.vsarychev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint {

    @Nullable
    @GetMapping("/{id}")
    @WebMethod
    Project get(@PathVariable("id") @Nullable String id);

    @PostMapping
    @WebMethod
    void post(@RequestBody @Nullable Project project);

    @PutMapping
    @WebMethod
    void put(@RequestBody @Nullable Project project);

    @DeleteMapping("/{id}")
    @WebMethod
    void delete(@PathVariable("id") @Nullable String id);
}
