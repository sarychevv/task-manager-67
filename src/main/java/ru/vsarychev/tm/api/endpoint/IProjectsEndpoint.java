package ru.vsarychev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.vsarychev.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectsEndpoint {
    @Nullable
    @GetMapping
    @WebMethod
    List<Project> get();

    @PostMapping
    @WebMethod
    void post(@RequestBody @Nullable List<Project> projects);

    @PutMapping
    @WebMethod
    void put(@RequestBody @Nullable List<Project> projects);

    @DeleteMapping
    @WebMethod
    void delete();
}
